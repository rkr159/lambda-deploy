const express = require('express')
const bodyParser = require('body-parser');
const cors = require('cors');


const app = express();
app.use(cors());
// Configuring body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
//const port = 3000;

app.get('/', (req, res) => {
    res.send('Hello World, from express');
});

app.post('/book', (req, res) => {
    const book = req.body;
    console.log(book);
    books.push(book);

    res.send('Book is added to the database');
});


module.exports = app;
//app.listen(port, () => console.log(`Hello world app listening on port ${port}!`))